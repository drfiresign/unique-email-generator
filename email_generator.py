#!/usr/bin/python

import argparse
import csv
import hashlib
from datetime import datetime

# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--count", help="number of addresses to generate; default is 100")
args = parser.parse_args()

# Set number of emails to generate
if not args.count:
    count = 100
else:
    count = args.count

# Email Generation


def generate_emails(count):

    domain = "@gmail.com"
    csv_title = str(count) + "_email_addresses.csv"
    with open(csv_title, "w") as csvfile:
        email_writer = csv.writer(csvfile, delimiter=',', quotechar='"',
                                  quoting=csv.QUOTE_MINIMAL)

        for i in range(0, int(count)):
            d = datetime.now()
            username = hashlib.sha224(str(d)).hexdigest()
            email = username + domain
            email_writer.writerow([email, 'J.', 'Doe'])

    print "{0} generated containing {1} emails.".format(csv_title, count)


if __name__ == "__main__":
    generate_emails(count)
