# Unique Email Generator

## TOC

1. Introduction
2. Usage
3. Feedback

## Introduction

This is not a fancy name for a project, but it's honestly such a small tool I
wasn't sure what else to call it. It generates unique email addresses based on
the SHA224 algorithm and puts them in a CSV file.

This was a tool created to help a friend test importing large lists of emails
addresses into various email marketing platforms such as MailChimp or
Squarespace Campaigns. It was built using Python 2 so it can be run by
non-technical users without making any modification to their current
environments.

## Usage

Run with `python email_generator.py`

It has a few options, but not many. Run without any arguments it will generate a
CSV of 100 email addresses. Each row contains a unique SHA224 generated email
username `@gmail.com`. Given the `-c` or `--count` argument, followed by a
number, the csv will consist of length `COUNT`.

Columns are Email, First Name, and Last Name, though they are not labeled as
such. In all cases first and last name will be J. Doe.

## Feedback

I don't have many more ideas for improvement as it wasn't extensively used by
more than one person. Possible future improvements may include:

* Specific flags for different email service formats.
* Randomized names inserted into list. You know, _spice things up_ a little.
* Randomized domains for email addresses to exist with.
* User specified seed variables for SHA generation.
* A complete rewrite of this project in JavaScript so it can be hosted as a
  service.

If you have a request or idea please open an Issue or PR. Thank you for reading!
